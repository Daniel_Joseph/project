/*
 * Date: September 1st 2014
 * Author: Yagnesh Shah   
 * Twitter handle: @YagneshHShah
 * Organization: Moolya Software Testing Pvt Ltd
 * License Type: MIT
 */


/*
Aim: Methods for Log-in/Log-out tests:
	validlLogin
	loginReliability&SigninAgain
	assertLoginElements
	maskedPasspword   
	emptyLogin
	partialFillLogin
	invalidLogin
	capsOnLogin (Suggestion: No message if caps lock is on)
	logoutSessionExpire

 */


package MakeMyTrip.methods;

import java.io.File;


import jxl.Cell;
import jxl.LabelCell;
import jxl.Sheet;
import jxl.Workbook;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import btac.sa.lib.FilesAndFolders;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;

public class LoginLogoutMethods  extends SelectBrowser {

	public static WebDriver driver;
	public LoginLogoutMethods(WebDriver driver){
		this.driver=driver;
	}

	static Workbook wrk1;
	static Sheet sheet1;
	static Cell colRow;

	
	
	@FindBy(xpath=".//*[@id='hp-widget__sfrom']")
	public static WebElement From;
	
	@FindBy(xpath=".//*[@id='hp-widget__sTo']")
	public static WebElement To;
	
	@FindBy(xpath=".//*[@id='hp-widget__depart']")
	public static WebElement Dept;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[3]/div/div[3]/div/div[1]/table/tbody/tr[2]/td[5]")
	public static WebElement DeptDate;
	
	@FindBy(xpath=".//*[@id='hp-widget__return']")
	public static WebElement Ret;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[3]/div/div[4]/div/div[1]/table/tbody/tr[3]/td[1]")
	public static WebElement RetDate;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[1]/section/div[2]/div[5]/input")
	public static WebElement Pass;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[3]/div/div[9]/div/div[1]/div[2]/ul/li[2]")
	public static WebElement PassSelect;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[1]/section/div[2]/div[6]/div[1]/input")
	public static WebElement Class;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[3]/div/div[10]/div[3]/label")
	public static WebElement ClassType;
	
	@FindBy(xpath="html/body/div[2]/div[3]/div[1]/section/div[2]/button")
	public static WebElement Ok;
	
	@FindBy(xpath=".//*[@id='header_tab_flights']")
	public static WebElement Flights;
	

	public static void validLogin() throws Exception
	{
		try
		{
//			Flights.click();
//			Thread.sleep(2000);
//			
			From.clear();
			
			
			From.sendKeys("Bangalore");
			Thread.sleep(2000);
			
			To.clear();
			
			
			To.sendKeys("Chennai");
			Thread.sleep(2000);
			
			//Dept.clear();
			
			
			Dept.click();
			Thread.sleep(2000);
			
			DeptDate.click();
			//Thread.sleep(2000);
			
			//Ret.clear();
			
			
			Ret.click();
			Thread.sleep(2000);
			
			RetDate.click();
			Thread.sleep(2000);
			
			//Pass.clear();
			
			
			Pass.click();
			Thread.sleep(2000);
			
			PassSelect.click();
			Thread.sleep(2000);
			
			//Class.clear();
			
			
			Class.click();
			Thread.sleep(2000);
			
			ClassType.click();
			Thread.sleep(2000);
			
			Ok.click();
			Thread.sleep(2000);
			
		}	
		catch(Exception e)
		{
			Reporter.log("----ghh--");
		}
		
		
	}

	
	
	

	
	

}
