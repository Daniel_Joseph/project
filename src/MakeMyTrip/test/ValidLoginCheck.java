/*
 * Date: September 1st 2014
 * Author: Yagnesh Shah   
 * Twitter handle: @YagneshHShah
 * Organization: Moolya Software Testing Pvt Ltd
 * License Type: MIT
*/


/*
Aim: Tests for Log-in/Log-out features. Following tests are covered in this script:
	assertLoginElements
	maskedPassword
	emptyLogin
	partialFillLogin
	invalidLogin
	capsOnLogin (Suggestion: No message displayed if caps lock is on)
	validlLogin
	loginReliabilityAndSigninAgain	
	logoutSessionExpire
*/

package MakeMyTrip.test;

import org.openqa.selenium.WebDriver;


import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import MakeMyTrip.methods.LoginLogoutMethods;
import btac.sa.lib.BrowserActions;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;
	

public class ValidLoginCheck extends SelectBrowser
{
	WebCommonMethods general;
	LoginLogoutMethods loginLogout;
	BrowserActions ba;
	MouseAndKeyboard mk;

	String appserver;
	
	@BeforeMethod
    public void openTheBrowser() throws Exception 
    {
    	WebDriver d = getBrowser();
	    loginLogout = PageFactory.initElements(d, LoginLogoutMethods.class);
	    general = PageFactory.initElements(d, WebCommonMethods.class);	    
	    ba = PageFactory.initElements(d, BrowserActions.class);    
	    mk = PageFactory.initElements(d, MouseAndKeyboard.class);	    

	    BrowserActions.openURLBasedOnDbDomain();
    } 

	//validLogin test 
	@Test(priority=1, groups={"SEARCH"})
	public void SEARCH() throws Exception
	{	
		System.out.println("************************SEARCH***************************");
		
		LoginLogoutMethods.validLogin();
		System.out.println("++++++++++++++++++++++++SEARCH success+++++++++++++++++++++++++++");
	}

	
	


}
